// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBzF_Uoj6bV73QT5bAR_bnNmoWkIVdWelE",
  authDomain: "shop-a996f.firebaseapp.com",
  projectId: "shop-a996f",
  storageBucket: "shop-a996f.appspot.com",
  messagingSenderId: "505239189481",
  appId: "1:505239189481:web:010fb5fcede99c0d9c3d78"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;